# Hebrew translation for lomiri-keyboard
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the lomiri-keyboard package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-keyboard\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-18 19:28+0000\n"
"PO-Revision-Date: 2020-12-24 08:57+0000\n"
"Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>\n"
"Language-Team: Hebrew <https://translate.ubports.com/projects/ubports/"
"keyboard-component/he/>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n == 1) ? 0 : ((n == 2) ? 1 : ((n > 10 && "
"n % 10 == 0) ? 2 : 3));\n"
"X-Generator: Weblate 3.11.3\n"
"X-Launchpad-Export-Date: 2017-04-07 06:31+0000\n"

#: ../qml/ActionsToolbar.qml:56
msgid "Select All"
msgstr "לבחור הכול"

#: ../qml/ActionsToolbar.qml:57
msgid "Redo"
msgstr "ביצוע חוזר"

#: ../qml/ActionsToolbar.qml:58
msgid "Undo"
msgstr "החזרה"

#: ../qml/ActionsToolbar.qml:73
msgid "Paste"
msgstr "הדבקה"

#: ../qml/ActionsToolbar.qml:74
msgid "Copy"
msgstr "העתקה"

#: ../qml/ActionsToolbar.qml:75
msgid "Cut"
msgstr "גזירה"

#: ../qml/FloatingActions.qml:62
msgid "Done"
msgstr "סיום"

#: ../qml/Keyboard.qml:351
msgid "Swipe to move selection"
msgstr "יש להחליק כדי להעביר את הבחירה"

#: ../qml/Keyboard.qml:352
msgid "Swipe to move cursor"
msgstr "יש להחליק כדי להעביר את הסמן"

#: ../qml/Keyboard.qml:352
msgid "Double-tap to enter selection mode"
msgstr "נגיעה כפולה כדי להיכנס למצב בחירה"

#: ../qml/keys/LanguageMenu.qml:106
msgid "Settings"
msgstr "הגדרות"

#: ../qml/keys/languages.js:19
msgid "Arabic"
msgstr "ערבית"

#: ../qml/keys/languages.js:20
msgid "Azerbaijani"
msgstr "אזרית"

#: ../qml/keys/languages.js:21
msgid "Bulgarian"
msgstr "בולגרית"

#: ../qml/keys/languages.js:22
msgid "Bosnian"
msgstr "בוסנית"

#: ../qml/keys/languages.js:23
msgid "Catalan"
msgstr "קטלאנית"

#: ../qml/keys/languages.js:24
msgid "Czech"
msgstr "צ׳כית"

#: ../qml/keys/languages.js:25
msgid "Danish"
msgstr "דנית"

#: ../qml/keys/languages.js:26
msgid "German"
msgstr "גרמנית"

#: ../qml/keys/languages.js:27
msgid "Emoji"
msgstr "אימוג׳י"

#: ../qml/keys/languages.js:28
msgid "Greek"
msgstr "יוונית"

#: ../qml/keys/languages.js:29
msgid "English"
msgstr "אנגלית"

#: ../qml/keys/languages.js:30
msgid "Esperanto"
msgstr "אספרנטו"

#: ../qml/keys/languages.js:31
msgid "Spanish"
msgstr "ספרדית"

#: ../qml/keys/languages.js:32
msgid "Persian"
msgstr "פרסית"

#: ../qml/keys/languages.js:33
msgid "Finnish"
msgstr "פינית"

#: ../qml/keys/languages.js:34
msgid "French"
msgstr "צרפתית"

#: ../qml/keys/languages.js:35
msgid ""
"French\n"
"(Swiss)"
msgstr ""
"צרפתית\n"
"(שווייץ)"

#: ../qml/keys/languages.js:36
msgid "Scottish Gaelic"
msgstr "גאלית סקוטית"

#: ../qml/keys/languages.js:37
msgid "Hebrew"
msgstr "עברית"

#: ../qml/keys/languages.js:38
msgid "Croatian"
msgstr "קרואטית"

#: ../qml/keys/languages.js:39
msgid "Hungarian"
msgstr "הונגרית"

#: ../qml/keys/languages.js:40
msgid "Icelandic"
msgstr "איסלנדית"

#: ../qml/keys/languages.js:41
msgid "Italian"
msgstr "איטלקית"

#: ../qml/keys/languages.js:42
msgid "Japanese"
msgstr "יפנית"

#: ../qml/keys/languages.js:43
msgid "Lithuanian"
msgstr "ליטאית"

#: ../qml/keys/languages.js:44
msgid "Latvian"
msgstr "לטבית"

#: ../qml/keys/languages.js:45
msgid "Korean"
msgstr "קוריאנית"

#: ../qml/keys/languages.js:46
msgid "Dutch"
msgstr "הולנדית"

#: ../qml/keys/languages.js:47
msgid "Norwegian"
msgstr "נורבגית"

#: ../qml/keys/languages.js:48
msgid "Polish"
msgstr "פולנית"

#: ../qml/keys/languages.js:49
msgid "Portuguese"
msgstr "פורטוגלית"

#: ../qml/keys/languages.js:50
msgid "Romanian"
msgstr "רומנית"

#: ../qml/keys/languages.js:51
msgid "Russian"
msgstr "רוסית"

#: ../qml/keys/languages.js:52
msgid "Slovenian"
msgstr "סלובנית"

#: ../qml/keys/languages.js:53
msgid "Serbian"
msgstr "סרבית"

#: ../qml/keys/languages.js:54
msgid "Swedish"
msgstr "שבדית"

#: ../qml/keys/languages.js:55
msgid "Turkish"
msgstr "טורקית"

#: ../qml/keys/languages.js:56
msgid "Ukrainian"
msgstr "אוקראינית"

#: ../qml/keys/languages.js:57
msgid ""
"Chinese\n"
"(Pinyin)"
msgstr ""
"סינית\n"
"(פִּין-יִין)"

#: ../qml/keys/languages.js:58
msgid ""
"Chinese\n"
"(Chewing)"
msgstr ""
"סינית\n"
"(צ׳ואינג)"
